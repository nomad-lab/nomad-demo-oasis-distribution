# We use the latest (i.e. develop brach) version of the nomad-fair image as a base
FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-fair:develop

USER root
# We install git because we need it to install git-based plugins
RUN apt-get update
RUN apt-get -y install git

# We install all plugins maintained in the plugins.txt file
COPY plugins.txt plugins.txt
RUN pip install --upgrade pip
RUN pip install -r plugins.txt

USER nomad